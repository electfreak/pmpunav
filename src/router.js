import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/:category',
      component: () => import('./views/Category.vue'),
    },


    {
      // ошибка
      path: '/:category/:item',
      name: 'category',
      component: () => import('./views/Category.vue'),
    },

    // {
    //   path: '/profession/:item',
    //   name: 'profession',
    //   component: () => import('./views/Profession.vue'),

    // }
  ]
})
